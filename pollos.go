package main

import "fmt"
import "os"

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

func main() {
    var mapa[10] string
    var copia[10][19] int
    for i := 0; i < 10; i++ {
        var line string
        fmt.Scan(&line)
        //Imprimir el mapa
        fmt.Fprintln(os.Stderr, line)
        mapa[i] = line
    }
    
    for i := 0; i < 10; i++ {
        for j := 0; j < 19; j++ {
            if mapa[i][j]==35 {
                copia[i][j]=9                
            }else if mapa[i][j]==46{
                copia[i][j]=-1
            }
        }
        
    }
    fmt.Fprintln(os.Stderr, copia)
    
    var robotCount int
    fmt.Scan(&robotCount)
    //Imprime el numero de pollos
    fmt.Fprintln(os.Stderr, robotCount)
    
    //Se guardan las posiciones de los robots
    posicion:= make([][]int,robotCount)
    for i := 0; i < robotCount; i++ {
        posicion[i]=make([]int,3)
        for j := 0; j < 3; j++ {
            posicion[i][j] = -1
        }
    }

    for i := 0; i < robotCount; i++ {
        var x, y int
        var direction string
        fmt.Scan(&x, &y, &direction)
        aux:=0
        switch direction{
        case "U":
            aux= 1
        case "D":
            aux= 2
        case "L":
            aux= 3
        case "R":
            aux= 4
        default:
            fmt.Fprintln(os.Stderr,"Not valid")
        }
        posicion[i][0]=x
        posicion[i][1]=y
        posicion[i][2]=aux    
    }
    //Se imprimen las posiciones de los robots
    fmt.Fprintln(os.Stderr,posicion)
    
    for i := 0; i < robotCount; i++{
        
    }

    
   
    
    //aux1:= strconv.Atoi(posicion[0][2])
    //fmt.Println(aux1)
    //fmt.Fprintln(os.Stderr, posicion)
    //fmt.Fprintln(os.Stderr,mapa[0][4])
    //fmt.Fprintln(os.Stderr,mapa[4][6])    
    // fmt.Fprintln(os.Stderr, "Debug messages...")
    //fmt.Println("0 0 U 1 1 R 2 2 D 3 3 L")// Write action to stdout
    fmt.Println()
}


func verificarCasilla(lugar []int, mapa [10]string) {
    var estado, inicio bool = false, true
    for estado == false{
        switch lugar[2]{
        case 1:
            if mapa[lugar[0]][lugar[1]-1]==35{
                lugar[2]++
                inicio=false
            }else{
                if inicio==true{
                    estado=true
                }else{
                    imprimirFlecha(lugar)
                    estado=true
                }              
            }
        case 2:
            if mapa[lugar[0]][lugar[1]+1]==35{
                lugar[2]++
                inicio=false
            }else{
                 if inicio==true{
                    estado=true
                }else{
                    imprimirFlecha(lugar)
                    estado=true
                }
            }
        case 3:
            if mapa[lugar[0]-1][lugar[1]]==35{
                lugar[2]++
                inicio=false
            }else{
                 if inicio==true{
                    estado=true
                }else{
                    imprimirFlecha(lugar)
                    estado=true
                }
            }
        case 4:
            if mapa[lugar[0]+1][lugar[1]]==35{
                lugar[2]=1    
                inicio=false
            }else{
                 if inicio==true{
                    estado=true
                }else{
                    imprimirFlecha(lugar)
                    estado=true
                }
            }
        }
    }
    
}


func imprimirFlecha(lugar []int){
    switch lugar[2]{
    case 1:
        fmt.Print(lugar[0], " ",lugar[1], " U ")
    case 2:
        fmt.Print(lugar[0], " ",lugar[1], " D ")
    case 3:
        fmt.Print(lugar[0], " ",lugar[1], " L ")
    case 4: 
        fmt.Print(lugar[0], " ",lugar[1], " R ")
        
    }    
}


