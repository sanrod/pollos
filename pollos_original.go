package main

import "fmt"
import "os"

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

func main() {
    for i := 0; i < 10; i++ {
        var line string
        fmt.Scan(&line)
        fmt.Fprintln(os.Stderr, line)
    }
    var robotCount int
    fmt.Scan(&robotCount)
    fmt.Fprintln(os.Stderr, robotCount)
    
    for i := 0; i < robotCount; i++ {
        var x, y int
        var direction string
        fmt.Scan(&x, &y, &direction)
        fmt.Fprintln(os.Stderr,x,y,direction)        
    }
    
    // fmt.Fprintln(os.Stderr, "Debug messages...")
    fmt.Println("0 0 U 1 1 R 2 2 D 3 3 L")// Write action to stdout
}