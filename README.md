# A*CRAFT - Pollos

Concurso de Coding Games and Programming Challenges to Code Better

## Instructions

![img1](Imagenes o algo asi/Selección_022.png)
![img2](Imagenes o algo asi/Selección_023.png)
![img3](Imagenes o algo asi/Selección_024.png)

## Evidence of particiaption
In this part we add the links that show the final rank we got.

* [Jorge](Evidencias/JorgeSanchezRodriguez.pdf)

* [Perla](Evidencias/PerlaMaciel.pdf)

## Lincense

See [license](LICENSE).