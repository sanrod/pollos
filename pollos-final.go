package main
import "fmt"
import "os"


func main() {
    //************Inicia codigo para obtener y copiar el mapa************************
    var mapa[10] string
    var copia[10][19] int
    for i := 0; i < 10; i++ {
        var line string
        fmt.Scan(&line)
        fmt.Fprintln(os.Stderr, line)
        mapa[i] = line
    }
    
    for i := 0; i < 10; i++ {
        for j := 0; j < 19; j++ {
            if mapa[i][j]==35 {
                copia[i][j]=9                
            }else if mapa[i][j]==46{
                copia[i][j]=-1
            }
        }        
    }
    //*****************Termina codigo para obtener y copiar el mapa****************

    //*****************Inicia obtener # de robots, posiciones y direcciones**********
    var robotCount int
    fmt.Scan(&robotCount)
    //Imprime el numero de pollos
    //fmt.Fprintln(os.Stderr, robotCount)
    
    //Se guardan las posiciones de los robots
    posicion:= make([][]int,robotCount)
    for i := 0; i < robotCount; i++ {
        posicion[i]=make([]int,3)
        for j := 0; j < 3; j++ {
            posicion[i][j] = -1
        }
    }
     
    for i := 0; i < robotCount; i++ {
        var x, y int
        var direction string
        fmt.Scan(&x, &y, &direction)
        aux:=0
        switch direction{
        case "U":
            aux= 1
        case "L":
            aux= 2
        case "D":
            aux= 3
        case "R":
            aux= 4
        default:
            fmt.Fprintln(os.Stderr,"Not valid")
        }
        posicion[i][0]=x
        posicion[i][1]=y
        posicion[i][2]=aux    
    }
    //Imprime las posiciones de todos los pollos
    fmt.Fprintln(os.Stderr, posicion)
    //****************Termina obtener # de robots, posiciones y direcciones**********
    posicionOriginal:= make([][]int,robotCount)
    for i := 0; i < robotCount; i++ {
        posicionOriginal[i]=make([]int,3)
    }

    for i := 0; i < robotCount; i++ {
        for j := 0; j < 3; j++ {
            posicionOriginal[i][j]=posicion[i][j]
        }
        
    }
    banderaInicio:=true 

    for j := 0; j < robotCount; j++ {
        for i := 0; i < casillasPosibles(copia); i++ {
            for verificarCasillaSiguiente(posicion[j][:], copia)==false{
                ayuda:= cambiarDireccion(posicion[j][:]) 
                posicion[j][0]=ayuda[0]
                posicion[j][1]=ayuda[1]
                posicion[j][2]=ayuda[2]
                if verificarCasillaSiguiente(posicion[j][:], copia)==true{
                    agregarFlecha(posicion[j][:],posicionOriginal[j][:],banderaInicio)
                }
            }
            fmt.Fprintln(os.Stderr, posicion)
            ayuda1 := cambiarPosicion(posicion[j][:])
            fmt.Fprintln(os.Stderr, posicion)
            posicion[j][0]=ayuda1[0]
            posicion[j][1]=ayuda1[1]
            posicion[j][2]=ayuda1[2]
            banderaInicio=false
        }        
    }
        
    
    
    
    
    fmt.Println()

}

func cambiarPosicion(posicionActual []int) []int{
    switch posicionActual[2]{
    case 1:
        if posicionActual[1]==-1 {
            posicionActual[1]=9
        }else{
            posicionActual[1]--
        }        
    case 2:
        if posicionActual[0]==-1 {
            posicionActual[0]=18
        }else{
            posicionActual[0]--
        } 
    case 3:
        if posicionActual[1]==10 {
            posicionActual[1]=0
        }else{
            posicionActual[1]++
        } 
    case 4: 
        if posicionActual[0]==19 {
            posicionActual[0]=0
        }else{
            posicionActual[0]++
        }      
    }
    return posicionActual
}


func cambiarDireccion(posicionActual []int) []int{
    if posicionActual[2]==4{
        posicionActual[2]=1
    }else{
        posicionActual[2]++
    }   
    return posicionActual
}

func agregarFlecha(posicionActual []int, posicionOriginal []int, inicio bool) {
    if (posicionActual[0]==posicionOriginal[0] && posicionActual[1]==posicionOriginal[1] && inicio==true){
        switch posicionActual[2]{
        case 1:
            if posicionActual[1]==0 {
                posicionActual[1]=9
            }
            fmt.Print(posicionActual[0], " ",posicionActual[1], " U ")
        case 2:
            if posicionActual[0]==0 {
               posicionActual[0]=18
            }
            fmt.Print(posicionActual[0], " ",posicionActual[1], " L ")
        case 3:
            if posicionActual[1]==9 {
                posicionActual[1]=0
            }
            fmt.Print(posicionActual[0], " ",posicionActual[1], " D ")
        case 4: 
            if posicionActual[0]==18 {
                posicionActual[0]=0
            }
            fmt.Print(posicionActual[0], " ",posicionActual[1], " R ")        
        }
        
    }else if(posicionActual[0]==posicionOriginal[0] && posicionActual[1]==posicionOriginal[1] && inicio==false){
        fmt.Fprintln(os.Stderr, posicionActual)
        fmt.Fprintln(os.Stderr, posicionOriginal)
        fmt.Fprintln(os.Stderr, "No hace nada.")        
    }else{
        switch posicionActual[2]{
        case 1:
            fmt.Print(posicionActual[0], " ",posicionActual[1], " U ")
        case 2:
            fmt.Print(posicionActual[0], " ",posicionActual[1], " L ")
        case 3:
            fmt.Print(posicionActual[0], " ",posicionActual[1], " D ")
        case 4: 
            fmt.Print(posicionActual[0], " ",posicionActual[1], " R ")        
        }
    }       
}


func verificarCasillaSiguiente(posicionActual []int, mapa [10][19]int) bool{
    if posicionActual[0]+1 < 19 &&  posicionActual[0]-1 > 0{
        if posicionActual[1]+1 < 10 && posicionActual[1]-1 > 0{
            switch posicionActual[2]{
                case 1:
                    if mapa[posicionActual[1]-1][posicionActual[0]]==9 {
                        return false
                    }else{
                        return true
                    }
                case 2:
                    if mapa[posicionActual[1]][posicionActual[0]-1]==9 {
                        return false
                    }else{
                        return true
                    }
                case 3:
                    if mapa[posicionActual[1]+1][posicionActual[0]]==9 {
                        return false
                    }else{
                        return true
                    }
                case 4:
                    if mapa[posicionActual[1]][posicionActual[0]+1]==9 {
                        return false
                    }else{
                        return true
                    }
            }
        }
    }else {
        switch posicionActual[2]{
                case 1:
                    if posicionActual[1]==0{
                        if mapa[9][posicionActual[0]]==9{
                            return false
                        }else{
                            return true
                        }
                    }
                case 2:
                    if posicionActual[0]==0{
                        if mapa[posicionActual[1]][18]==9{
                            return false
                        }else{
                            return true
                        }
                    }e
                case 3:
                    if posicionActual[1]==9{
                        if mapa[0][posicionActual[0]]==9{
                            return false
                        }else{
                            return true
                        }
                    }
                case 4:
                    if posicionActual[1]==0{
                        if mapa[posicionActual[1]][0]==9{
                            return false
                        }else{
                            return true
                        }
                    }
            }
    }
    return true
}


func casillasPosibles(mapa [10][19]int) int{
    count:=0
    for i := 0; i < 10; i++ {
        for j := 0; j < 19; j++ {
            if mapa[i][j]==-1{
                count++
            }   
        }
    }
    return count
}



